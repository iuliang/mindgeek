<?php
declare(strict_types = 1);

use App\Http\Converters\SourceConverter;
use Illuminate\Support\Collection;

class SourceConverterTest extends TestCase
{
    public function testCreateSource()
    {
        // Given
        $sourceConverter = new SourceConverter();
        $sourceObject = new stdClass;
        $sourceObject->id = 1;
        $sourceObject->source_url = 'http://www.google.ro';
        $sourceObject->processed = false;

        // When
        $source = $sourceConverter->createSource($sourceObject);

        // Then
        $this->assertEquals(1, $source->getId());
        $this->assertEquals('http://www.google.ro', $source->getSourceURL());
        $this->assertEquals(false, $source->isProcessed());
    }

    public function testConvertSourceListToResult()
    {
        $sourceConverter = new SourceConverter();
        $sourceObject1 = new stdClass;
        $sourceObject1->id = 1;
        $sourceObject1->source_url = 'http://www.google.ro';
        $sourceObject1->processed = false;

        $sourceObject2 = new stdClass;
        $sourceObject2->id = 2;
        $sourceObject2->source_url = 'http://www.facebook.ro';
        $sourceObject2->processed = true;

        $collection = Collection::make();
        $collection->push($sourceObject1);
        $collection->push($sourceObject2);

        // When
        $result = $sourceConverter->convertSourceListToResult($collection);

        // Then
        $this->assertEquals(1, $result->first()->getId());
        $this->assertEquals('http://www.google.ro', $result->first()->getSourceURL());
        $this->assertEquals(false, $result->first()->isProcessed());
    }
}
