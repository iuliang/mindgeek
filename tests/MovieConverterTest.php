<?php
declare(strict_types = 1);

use App\Http\Converters\MovieConverter;

class MovieConverterTest extends TestCase
{
    public function testCreateSource()
    {
        // Given
        $movieConverter = new MovieConverter();
        $sourceArray = [];
        $sourceArray['body'] = 'Billy Crystal plays put-out-to-graze baseball commentator';
        $sourceArray['cardImages'] = [
            [
                'url' => 'https://mgtechtest.blob.core.windows.net/images/unscaled/2012/11/29/Parental-Guidance-VPA.jpg',
                'h' => 1004,
                'w' => 768
            ], [
                'url' => 'https://mgtechtest.blob.core.windows.net/images/unscaled/2013/07/15/LPA-Parental-guidance.jpg',
                'h' => 748,
                'w' => 1024
            ]
        ];
        $sourceArray['cast'] = [
            [
                'name' => 'Billy Crystal'
            ], [
                'name' => 'Bette Midler'
            ]
        ];
        $sourceArray['cert'] = 'U';
        $sourceArray['class'] = 'Movie';
        $sourceArray['directors'] = [
            [
                'name' => 'Andy Fickman'
            ]
        ];
        $sourceArray['duration'] = 5940;
        $sourceArray['genres'] = [
            'Comedy',
            'Family'
        ];
        $sourceArray['headline'] = 'Parental Guidance';
        $sourceArray['id'] = '8ad589013b496d9f013b4c0b684a4a5d';
        $sourceArray['keyArtImages'] = [
            [
                'url' => 'https://mgtechtest.blob.core.windows.net/images/unscaled/2012/12/19/Parental-Guidance-KA-KA-to-KP3.jpg',
                'h' => 169,
                'w' => 114
            ], [
                'url' => 'https://mgtechtest.blob.core.windows.net/images/unscaled/2012/12/19/Parental-Guidance-KA-KA-to-KP4.jpg',
                'h' => 338,
                'w' => 228
            ]
        ];
        $sourceArray['lastUpdated'] = '2013-07-15';
        $sourceArray['quote'] = 'an intriguing pairing of Bette Midler and Billy Crystal';
        $sourceArray['rating'] = 3;
        $sourceArray['reviewAuthor'] = 'Tim Evans';
        $sourceArray['skyGoId'] = 'd1bf901693832410VgnVCM1000000b43150a____';
        $sourceArray['skyGoUrl'] = 'http://go.sky.com/vod/content/GOPCMOVIES/RSS/Movies/content/assetId/6ba3fb6afd03e310VgnVCM1000000b43150a________/videoId/d1bf901693832410VgnVCM1000000b43150a________/content/playSyndicate.do';
        $sourceArray['sum'] = '66b14d5c58904900b13b404ae29eb7fe';
        $sourceArray['synopsis'] = 'When veteran baseball commentator Artie Decker (Billy Crystal)';
        $sourceArray['url'] = 'http://skymovies.sky.com/parental-guidance/review';
        $sourceArray['videos'] = [
            [
                'title' => 'Trailer - Parental Guidance',
                'alternatives' => [
                    [
                        'quality' => 'Low',
                        'url' => 'http://static.video.sky.com//skymovies/2012/11/44104/44104-270p_320K_H264.mp4'
                    ]
                ],
                'type' => 'trailer',
                'url' => 'http://static.video.sky.com//skymovies/2012/11/44104/44104-360p_800K_H264.mp4'
            ]
        ];
        $sourceArray['viewingWindow'] = [
            'startDate' => '2013-12-27',
            'wayToWatch' => 'Sky Movies',
            'endDate' => '2015-01-21'
        ];
        $sourceArray['year'] = 2012;

        // When
        $result = $movieConverter->convertDecodedToMovie($sourceArray);

        // Then
        $this->assertEquals('Billy Crystal plays put-out-to-graze baseball commentator', $result->getBody());
        $this->assertEquals('U', $result->getCert());
        $this->assertEquals(2, $result->getCardImages()->count());
        $this->assertEquals(1, $result->getDirectors()->count());
        $this->assertEquals('Parental Guidance', $result->getHeadline());
        $this->assertEquals('8ad589013b496d9f013b4c0b684a4a5d', $result->getExternalId());
        $this->assertEquals(2, $result->getKeyArtImages()->count());
        $this->assertEquals('Tim Evans', $result->getReviewAuthor());
    }
}
