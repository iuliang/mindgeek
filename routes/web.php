<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});

$router->get('/', 'MovieController@index');
$router->get('/sources', 'SourceController@index');
$router->get('/sources/import', 'SourceController@importStatic');
$router->get('/movies', 'MovieController@index');
$router->get('/directors', 'DirectorController@index');
$router->get('/casting', 'CastingController@index');
$router->get('/genres', 'GenreController@index');
