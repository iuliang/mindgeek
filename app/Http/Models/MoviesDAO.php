<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MoviesDAO
{
    public function saveMovie(Movie $movie)
    {
        $internalId = DB::table('movies')->insertGetId([
            'body' => $movie->getBody(),
            'cert' => $movie->getCert(),
            'class' => $movie->getClass(),
            'duration' => $movie->getDuration(),
            'headline' => $movie->getHeadline(),
            'external_id' => $movie->getExternalId(),
            'last_updated' => $movie->getLastUpdated()->format('Y-m-d'),
            'quote' => $movie->getQuote(),
            'rating' => $movie->getRating(),
            'review_author' => $movie->getReviewAuthor(),
            'sky_go_id' => $movie->getSkyGoId(),
            'sky_go_url' => $movie->getSkyGoUrl(),
            'sum' => $movie->getSum(),
            'synopsys' => $movie->getSynopsis(),
            'url' => $movie->getUrl(),
            'viewing_window' => $movie->getViewingWindow(),
            'year' => $movie->getYear()
        ]);

        $movie->setMovieKey(new MovieKey($internalId));
    }

    /**
     * @return Collection
     */
    public function getMoviesList(): Collection
    {
        return DB::table('movies')->get(['id', 'headline', 'rating', 'year']);
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getMovie(MovieKey $movieKey): Collection
    {
        return DB::table('movies')->where('id', $movieKey->getMovieId())->get();
    }

    /**
     * @param Movie $movie
     * @return bool
     */
    public function movieExists(Movie $movie): bool
    {
        return DB::table('movies')->where('external_id', $movie->getExternalId())->get()->first() !== null;
    }
}
