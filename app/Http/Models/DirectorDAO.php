<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\Movies\Director;
use App\Http\Entities\Movies\Keys\DirectorKey;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DirectorDAO
{
    public function saveDirector(Movie $movie)
    {
        /** @var Director $director */
        foreach ($movie->getDirectors() as $director) {
            $directorResult = DB::table('directors')->where('name', $director->getName())->get()->first();
            if ($directorResult === null) {
                $this->insertDirector($director);
            } else {
                $director->setDirectorKey(new DirectorKey((int)$directorResult->id));
            }

            /** @var DirectorKey $directorKey */
            $directorKey = $director->getDirectorKey();
            /** @var MovieKey $movieKey */
            $movieKey = $movie->getMovieKey();
            DB::table('director_movies')->insert([
                'movie_id' => $movieKey->getMovieId(),
                'director_id' => $directorKey->getDirectorId()
            ]);
        }
    }

    /**
     * @param Director $director
     */
    public function insertDirector(Director $director)
    {
        $internalId = DB::table('directors')->insertGetId([
                'name' => $director->getName()
        ]);
        $director->setDirectorKey(new DirectorKey($internalId));
    }

    /**
     * @return Collection
     */
    public function getDirectorsList(): Collection
    {
        return DB::table('directors')->get(['id', 'name']);
    }

    /**
     * @param DirectorKey $directorKey
     * @return Collection
     */
    public function getMovieListForDirector(DirectorKey $directorKey): Collection
    {
        return DB::table('director_movies')
            ->join('movies', 'movies.id', '=', 'director_movies.movie_id')
            ->join('directors', 'directors.id', '=', 'director_movies.director_id')
            ->where('director_id', $directorKey->getDirectorId())
            ->get(['director_id', 'name', 'movie_id', 'headline', 'year']);
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getDirectorListForMovie(MovieKey $movieKey): Collection
    {
        return DB::table('director_movies')
            ->join('directors', 'directors.id', '=', 'director_movies.director_id')
            ->where('movie_id', $movieKey->getMovieId())
            ->get(['director_id', 'name']);
    }
}
