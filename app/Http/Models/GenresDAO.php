<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\Movies\Gen;
use App\Http\Entities\Movies\Keys\GenreKey;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class GenresDAO
{
    public function saveGenres(Movie $movie)
    {
        /** @var Gen $gen */
        foreach ($movie->getGenres() as $gen) {
            $genResult = DB::table('genres')->where('type', $gen->getType())->get()->first();
            if ($genResult === null) {
                $this->insertGen($gen);
            } else {
                $gen->setGenreKey(new GenreKey((int)$genResult->id));
            }

            /** @var GenreKey $genreKey */
            $genreKey = $gen->getGenreKey();
            /** @var MovieKey $movieKey */
            $movieKey = $movie->getMovieKey();
            DB::table('genres_movies')->insert([
                'movie_id' => $movieKey->getMovieId(),
                'genre_id' => $genreKey->getGenreId()
            ]);
        }
    }

    /**
     * @param Gen $gen
     */
    public function insertGen(Gen $gen)
    {
        $internalId = DB::table('genres')->insertGetId([
            'type' => $gen->getType()
        ]);
        $gen->setGenreKey(new GenreKey($internalId));
    }

    /**
     * @return Collection
     */
    public function getGenresList(): Collection
    {
        return DB::table('genres')->get(['id', 'type']);
    }

    /**
     * @param GenreKey $genreKey
     * @return Collection
     */
    public function getMovieListForGenre(GenreKey $genreKey): Collection
    {
        return DB::table('genres_movies')
            ->join('movies', 'movies.id', '=', 'genres_movies.movie_id')
            ->join('genres', 'genres.id', '=', 'genres_movies.genre_id')
            ->where('genre_id', $genreKey->getGenreId())
            ->get(['genre_id', 'type', 'movie_id', 'headline', 'year']);
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getGenreListForMovie(MovieKey $movieKey): Collection
    {
        return DB::table('genres_movies')
            ->join('genres', 'genres.id', '=', 'genres_movies.genre_id')
            ->where('movie_id', $movieKey->getMovieId())
            ->get(['genre_id', 'type']);
    }
}
