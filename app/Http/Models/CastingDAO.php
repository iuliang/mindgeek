<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\Movies\Cast;
use App\Http\Entities\Movies\Keys\CastKey;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CastingDAO
{
    public function saveCasting(Movie $movie)
    {
        /** @var Cast $cast */
        foreach ($movie->getCasts() as $cast) {
            $castResult = DB::table('casting')->where('name', $cast->getName())->get()->first();
            if ($castResult === null) {
                $this->insertCasting($cast);
            } else {
                $cast->setCastKey(new CastKey((int)$castResult->id));
            }

            /** @var CastKey $castKey */
            $castKey = $cast->getCastKey();
            /** @var MovieKey $movieKey */
            $movieKey = $movie->getMovieKey();
            DB::table('casting_movies')->insert([
                'movie_id' => $movieKey->getMovieId(),
                'casting_id' => $castKey->getCastId()
            ]);
        }
    }

    /**
     * @param Cast $cast
     */
    public function insertCasting(Cast $cast)
    {
        $internalId = DB::table('casting')->insertGetId([
            'name' => $cast->getName()
        ]);
        $cast->setCastKey(new CastKey($internalId));
    }

    /**
     * @return Collection
     */
    public function getCastingList(): Collection
    {
        return DB::table('casting')->get(['id', 'name']);
    }

    /**
     * @param CastKey $castKey
     * @return Collection
     */
    public function getMovieListForCasting(CastKey $castKey): Collection
    {
        return DB::table('casting_movies')
            ->join('movies', 'movies.id', '=', 'casting_movies.movie_id')
            ->join('casting', 'casting.id', '=', 'casting_movies.casting_id')
            ->where('casting_id', $castKey->getCastId())
            ->get(['casting_id', 'name', 'movie_id', 'headline', 'year']);
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getCastingListForMovie(MovieKey $movieKey): Collection
    {
        return DB::table('casting_movies')
            ->join('casting', 'casting.id', '=', 'casting_movies.casting_id')
            ->where('movie_id', $movieKey->getMovieId())
            ->get(['casting_id', 'name']);
    }
}
