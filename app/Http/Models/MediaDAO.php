<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\MediaStorage;
use App\Http\Entities\Movies\CardImage;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MediaDAO
{
    const CARD_IMAGES_PATH = 'card_images';
    const KEY_ART_IMAGES_PATH = 'key_art_images';
    const VIDEOS_PATH = 'videos';

    public function saveCardImages(Movie $movie)
    {
        $cardImagesFolder = MediaStorage::getMediaStorage() . $movie->getExternalId() . DIRECTORY_SEPARATOR . self::CARD_IMAGES_PATH;
        if (!file_exists($cardImagesFolder)) {
            mkdir($cardImagesFolder, 0777, true);
        }

        foreach ($movie->getCardImages() as $cardImage) {
            $headers = get_headers($cardImage->getUrl(), 1);
            if (!\in_array($headers['Content-Type'], MediaStorage::getAllowedImagesTypes(), true)) {
                continue;
            }

            $imageUrlElements = explode('/', $cardImage->getUrl());
            $imageName = end($imageUrlElements);

            $cacheFileName = $cardImagesFolder . DIRECTORY_SEPARATOR . $imageName;
            if (!file_exists($cacheFileName)) {
                copy($cardImage->getUrl(), $cacheFileName);
            }
        }
    }

    public function saveKeyArtImages(Movie $movie)
    {
        $keyArtImagesFolder = MediaStorage::getMediaStorage() . $movie->getExternalId() . DIRECTORY_SEPARATOR . self::KEY_ART_IMAGES_PATH;
        if (!file_exists($keyArtImagesFolder)) {
            mkdir($keyArtImagesFolder, 0777, true);
        }

        foreach ($movie->getKeyArtImages() as $keyArtImage) {
            $headers = get_headers($keyArtImage->getUrl(), 1);
            if (!\in_array($headers['Content-Type'], MediaStorage::getAllowedImagesTypes(), true)) {
                continue;
            }

            $imageUrlElements = explode('/', $keyArtImage->getUrl());
            $imageName = end($imageUrlElements);

            $cacheFileName = $keyArtImagesFolder . DIRECTORY_SEPARATOR . $imageName;
            if (!file_exists($cacheFileName)) {
                copy($keyArtImage->getUrl(), $cacheFileName);
            }
        }
    }

    public function saveVideos(Movie $movie)
    {
        $videos = MediaStorage::getMediaStorage() . $movie->getExternalId() . DIRECTORY_SEPARATOR . self::VIDEOS_PATH;
        if (!file_exists($videos)) {
            mkdir($videos, 0777, true);
        }

        foreach ($movie->getVideos() as $video) {
            $headers = get_headers($video->getUrl(), 1);
            if (!\in_array($headers['Content-Type'], MediaStorage::getAllowedVideos(), true)) {
                continue;
            }

            $videoUrlElements = explode('/', $video->getUrl());
            $videoName = end($videoUrlElements);

            $cacheFileName = $videos . DIRECTORY_SEPARATOR . $videoName;
            if (!file_exists($cacheFileName)) {
                copy($video->getUrl(), $cacheFileName);
            }
        }
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getCardImagesForMovie(MovieKey $movieKey): Collection
    {
        $result = Collection::make();
        $movieExternalIdRaw = DB::table('movies')->where('id', $movieKey->getMovieId())->get()->first();
        if ($movieExternalIdRaw !== null){
            $externalId = $movieExternalIdRaw->external_id;

            $fileFolder = MediaStorage::getMediaStorage() . $externalId . DIRECTORY_SEPARATOR . self::CARD_IMAGES_PATH;

            $files = scandir($fileFolder, 1);
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }

                $fileFolder = 'media_files' . DIRECTORY_SEPARATOR . $externalId . DIRECTORY_SEPARATOR . self::CARD_IMAGES_PATH;
                $cardImageURL = $fileFolder . DIRECTORY_SEPARATOR . $file;
                $cardImage = new CardImage($cardImageURL);
                $result->push($cardImage);
            }

        }

        return $result;
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getKeyArtImagesForMovie(MovieKey $movieKey): Collection
    {
        $result = Collection::make();
        $movieExternalIdRaw = DB::table('movies')->where('id', $movieKey->getMovieId())->get()->first();
        if ($movieExternalIdRaw !== null){
            $externalId = $movieExternalIdRaw->external_id;

            $fileFolder = MediaStorage::getMediaStorage() . $externalId . DIRECTORY_SEPARATOR . self::KEY_ART_IMAGES_PATH;

            $files = scandir($fileFolder, 1);
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }

                $fileFolder = 'media_files' . DIRECTORY_SEPARATOR . $externalId . DIRECTORY_SEPARATOR . self::KEY_ART_IMAGES_PATH;
                $cardImageURL = $fileFolder . DIRECTORY_SEPARATOR . $file;
                $cardImage = new CardImage($cardImageURL);
                $result->push($cardImage);
            }

        }

        return $result;
    }

    /**
     * @param MovieKey $movieKey
     * @return Collection
     */
    public function getVideosForMovie(MovieKey $movieKey): Collection
    {
        $result = Collection::make();
        $movieExternalIdRaw = DB::table('movies')->where('id', $movieKey->getMovieId())->get()->first();
        if ($movieExternalIdRaw !== null){
            $externalId = $movieExternalIdRaw->external_id;

            $fileFolder = MediaStorage::getMediaStorage() . $externalId . DIRECTORY_SEPARATOR . self::VIDEOS_PATH;

            $files = scandir($fileFolder, 1);
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }

                $fileFolder = 'media_files' . DIRECTORY_SEPARATOR . $externalId . DIRECTORY_SEPARATOR . self::VIDEOS_PATH;
                $cardImageURL = $fileFolder . DIRECTORY_SEPARATOR . $file;
                $cardImage = new CardImage($cardImageURL);
                $result->push($cardImage);
            }

        }

        return $result;
    }
}
