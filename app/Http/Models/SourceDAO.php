<?php
declare(strict_types = 1);

namespace App\Http\Models;

use App\Http\Entities\Source\Source;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SourceDAO
{
    public function getSources(): Collection
    {
        return DB::table('sources')->get();
    }

    /**
     * @param int $sourceId
     * @return Collection
     */
    public function getSource(int $sourceId): Collection
    {
        return DB::table('sources')->where('id', $sourceId)->get();
    }

    /**
     * @param Source $source
     */
    public function updateSourceStatus(Source $source)
    {
        DB::table('sources')->where('id', $source->getId())->update(['processed' => 1]);
    }
}
