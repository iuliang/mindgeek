<?php
declare(strict_types = 1);

namespace App\Http\Entities;

class MediaStorage
{
    /**
     * @return string
     */
    public static function getMediaStorage(): string
    {
        return base_path() . 'public'.DIRECTORY_SEPARATOR.'media_files'.DIRECTORY_SEPARATOR;
    }

    /**
     * @return array
     */
    public static function getAllowedImagesTypes(): array
    {
        return [
            'image/jpeg',
            'image/png',
            'image/gif'
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedVideos(): array {
        return [
            'video/mp4'
        ];
    }
}
