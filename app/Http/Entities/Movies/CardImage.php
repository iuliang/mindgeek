<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

class CardImage
{
    /** @var string */
    private $url;
    /** @var int|null */
    private $height;
    /** @var int|null */
    private $width;

    /**
     * CardImage constructor.
     * @param string $url
     * @param int $height
     * @param int $width
     */
    public function __construct(
        string $url,
        int $height = null,
        int $width = null
    ) {
        $this->url = $url;
        $this->height = $height;
        $this->width = $width;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return int|null
     */
    public function getWidth()
    {
        return $this->width;
    }
}
