<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

class Video
{
    /** @var string */
    private $title;
    /** @var string */
    private $url;
    /** @var string */
    private $type;

    /**
     * CardImage constructor.
     * @param string $title
     * @param string $url
     * @param string|null $type
     */
    public function __construct(
        string $title,
        string $url,
        string $type = null
    ) {
        $this->title = $title;
        $this->url = $url;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
}
