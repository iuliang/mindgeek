<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

use App\Http\Entities\Movies\Keys\MovieKey;
use DateTime;
use Illuminate\Support\Collection;

class Movie
{
    /** @var string */
    private $body;
    /** @var Collection */
    private $cardImages;
    /** @var Collection */
    private $casts;
    /** @var string */
    private $cert;
    /** @var string */
    private $class;
    /** @var Collection */
    private $directors;
    /** @var int */
    private $duration;
    /** @var Collection */
    private $genres;
    /** @var string */
    private $headline;
    /** @var string */
    private $externalId;
    /** @var Collection */
    private $keyArtImages;
    /** @var DateTime */
    private $lastUpdated;
    /** @var string */
    private $quote;
    /** @var int */
    private $rating;
    /** @var string */
    private $reviewAuthor;
    /** @var string */
    private $skyGoId;
    /** @var string */
    private $skyGoUrl;
    /** @var string */
    private $sum;
    /** @var string */
    private $synopsis;
    /** @var string */
    private $url;
    /** @var Collection */
    private $videos;
    /** @var string */
    private $viewingWindow;
    /** @var int */
    private $year;
    /** @var MovieKey */
    private $movieKey;

    /**
     * Movie constructor.
     * @param string $body
     * @param Collection $cardImages
     * @param Collection $cast
     * @param string $cert
     * @param string $class
     * @param Collection $directors
     * @param int $duration
     * @param Collection $genres
     * @param string $headline
     * @param string $externalId
     * @param Collection $keyArtImages
     * @param DateTime $lastUpdated
     * @param string $quote
     * @param int $rating
     * @param string $reviewAuthor
     * @param string $skyGoId
     * @param string $skyGoUrl
     * @param string $sum
     * @param string $synopsis
     * @param string $url
     * @param Collection $videos
     * @param string $viewingWindow
     * @param int $year
     * @param MovieKey|null $movieKey
     */
    public function __construct(
        string $body,
        Collection $cardImages,
        Collection $cast,
        string $cert,
        string $class,
        Collection $directors,
        int $duration,
        Collection $genres,
        string $headline,
        string $externalId,
        Collection $keyArtImages,
        DateTime $lastUpdated,
        string $quote,
        int $rating,
        string $reviewAuthor,
        string $skyGoId,
        string $skyGoUrl,
        string $sum,
        string $synopsis,
        string $url,
        Collection $videos,
        string $viewingWindow,
        int $year,
        MovieKey $movieKey = null
    ) {
        $this->body = $body;
        $this->cardImages = $cardImages;
        $this->casts = $cast;
        $this->cert = $cert;
        $this->class = $class;
        $this->directors = $directors;
        $this->duration = $duration;
        $this->genres = $genres;
        $this->headline = $headline;
        $this->externalId = $externalId;
        $this->keyArtImages = $keyArtImages;
        $this->lastUpdated = $lastUpdated;
        $this->quote = $quote;
        $this->rating = $rating;
        $this->reviewAuthor = $reviewAuthor;
        $this->skyGoId = $skyGoId;
        $this->skyGoUrl = $skyGoUrl;
        $this->sum = $sum;
        $this->synopsis = $synopsis;
        $this->url = $url;
        $this->videos = $videos;
        $this->viewingWindow = $viewingWindow;
        $this->year = $year;
        $this->movieKey = $movieKey;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return Collection
     */
    public function getCardImages(): Collection
    {
        return $this->cardImages;
    }

    /**
     * @return Collection
     */
    public function getCasts(): Collection
    {
        return $this->casts;
    }

    /**
     * @return string
     */
    public function getCert(): string
    {
        return $this->cert;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return Collection
     */
    public function getDirectors(): Collection
    {
        return $this->directors;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @return Collection
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    /**
     * @return string
     */
    public function getHeadline(): string
    {
        return $this->headline;
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->externalId;
    }

    /**
     * @return Collection
     */
    public function getKeyArtImages(): Collection
    {
        return $this->keyArtImages;
    }

    /**
     * @return DateTime
     */
    public function getLastUpdated(): DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @return string
     */
    public function getQuote(): string
    {
        return $this->quote;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getReviewAuthor(): string
    {
        return $this->reviewAuthor;
    }

    /**
     * @return string
     */
    public function getSkyGoId(): string
    {
        return $this->skyGoId;
    }

    /**
     * @return string
     */
    public function getSkyGoUrl(): string
    {
        return $this->skyGoUrl;
    }

    /**
     * @return string
     */
    public function getSum(): string
    {
        return $this->sum;
    }

    /**
     * @return string
     */
    public function getSynopsis(): string
    {
        return $this->synopsis;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return Collection
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    /**
     * @return string
     */
    public function getViewingWindow(): string
    {
        return $this->viewingWindow;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @return MovieKey|null
     */
    public function getMovieKey()
    {
        return $this->movieKey;
    }

    /**
     * @param MovieKey $movieKey
     */
    public function setMovieKey(MovieKey $movieKey)
    {
        $this->movieKey = $movieKey;
    }
}
