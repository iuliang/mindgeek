<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies\Keys;

use App\Http\Entities\EntityKey;

class GenreKey extends EntityKey
{
    public function __construct(int $genreId)
    {
        parent::__construct('GENRE', $genreId);
    }

    /**
     * @return int
     */
    public function getGenreId(): int
    {
        return parent::getKeyId();
    }
}
