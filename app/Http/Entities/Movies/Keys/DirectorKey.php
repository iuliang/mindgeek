<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies\Keys;

use App\Http\Entities\EntityKey;

class DirectorKey extends EntityKey
{
    public function __construct(int $directorId)
    {
        parent::__construct('DIRECTOR', $directorId);
    }

    /**
     * @return int
     */
    public function getDirectorId(): int
    {
        return parent::getKeyId();
    }
}
