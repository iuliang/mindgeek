<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies\Keys;

use App\Http\Entities\EntityKey;

class CastKey extends EntityKey
{
    public function __construct(int $castId)
    {
        parent::__construct('CAST', $castId);
    }

    /**
     * @return int
     */
    public function getCastId(): int
    {
        return parent::getKeyId();
    }
}
