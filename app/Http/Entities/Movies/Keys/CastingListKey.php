<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies\Keys;

use App\Http\Entities\EntityKey;

class CastingListKey extends EntityKey
{
    public function __construct()
    {
        parent::__construct('CASTING_LIST', '-');
    }
}
