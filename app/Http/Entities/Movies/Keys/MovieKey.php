<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies\Keys;

use App\Http\Entities\EntityKey;

class MovieKey extends EntityKey
{
    public function __construct(int $movieId)
    {
        parent::__construct('MOVIE', $movieId);
    }

    /**
     * @return int
     */
    public function getMovieId(): int
    {
        return parent::getKeyId();
    }
}
