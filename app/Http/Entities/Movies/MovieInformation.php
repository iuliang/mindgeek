<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

use Illuminate\Support\Collection;

class MovieInformation
{
    /** @var Collection */
    private $movieData;
    /** @var Collection */
    private $movieCast;
    /** @var Collection */
    private $movieDirector;
    /** @var Collection */
    private $movieGenre;
    /** @var Collection */
    private $cardImages;
    /** @var Collection */
    private $keyArtImages;
    /** @var Collection */
    private $videos;

    /**
     * MovieInformation constructor.
     * @param Collection $movieData
     * @param Collection $movieCast
     * @param Collection $movieDirector
     * @param Collection $movieGenre
     * @param Collection $cardImages
     * @param Collection $keyArtImages
     * @param Collection $videos
     */
    public function __construct(
        Collection $movieData,
        Collection $movieCast,
        Collection $movieDirector,
        Collection $movieGenre,
        Collection $cardImages,
        Collection $keyArtImages,
        Collection $videos
    ) {
        $this->movieData = $movieData;
        $this->movieCast = $movieCast;
        $this->movieDirector = $movieDirector;
        $this->movieGenre = $movieGenre;
        $this->cardImages = $cardImages;
        $this->keyArtImages = $keyArtImages;
        $this->videos = $videos;
    }

    /**
     * @return Collection
     */
    public function getMovieData(): Collection
    {
        return $this->movieData;
    }

    /**
     * @return Collection
     */
    public function getMovieCast(): Collection
    {
        return $this->movieCast;
    }

    /**
     * @return Collection
     */
    public function getMovieDirector(): Collection
    {
        return $this->movieDirector;
    }

    /**
     * @return Collection
     */
    public function getMovieGenre(): Collection
    {
        return $this->movieGenre;
    }

    /**
     * @return Collection
     */
    public function getCardImages(): Collection
    {
        return $this->cardImages;
    }

    /**
     * @return Collection
     */
    public function getKeyArtImages(): Collection
    {
        return $this->keyArtImages;
    }

    /**
     * @return Collection
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }
}
