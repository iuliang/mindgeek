<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

use App\Http\Entities\Movies\Keys\DirectorKey;

class Director
{
    /** @var string */
    private $name;
    /** @var DirectorKey */
    private $directorKey;

    /**
     * Cast constructor.
     * @param string $name
     * @param DirectorKey|null $directorKey
     */
    public function __construct(string $name, DirectorKey $directorKey = null)
    {
        $this->name = $name;
        $this->directorKey = $directorKey;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DirectorKey|null
     */
    public function getDirectorKey()
    {
        return $this->directorKey;
    }

    /**
     * @param DirectorKey $directorKey
     */
    public function setDirectorKey(DirectorKey $directorKey)
    {
        $this->directorKey = $directorKey;
    }
}
