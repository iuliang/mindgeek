<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

use App\Http\Entities\Movies\Keys\CastKey;

class Cast
{
    /** @var string */
    private $name;
    /** @var int */
    private $castKey;

    /**
     * Cast constructor.
     * @param string $name
     * @param CastKey|null $castKey
     */
    public function __construct(string $name, CastKey $castKey = null)
    {
        $this->name = $name;
        $this->castKey = $castKey;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return CastKey|null
     */
    public function getCastKey()
    {
        return $this->castKey;
    }

    /**
     * @param CastKey $castKey
     */
    public function setCastKey(CastKey $castKey)
    {
        $this->castKey = $castKey;
    }
}
