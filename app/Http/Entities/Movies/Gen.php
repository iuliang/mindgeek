<?php
declare(strict_types = 1);

namespace App\Http\Entities\Movies;

use App\Http\Entities\Movies\Keys\GenreKey;

class Gen
{
    /** @var string */
    private $type;
    /** @var GenreKey */
    private $genreKey;

    /**
     * Cast constructor.
     * @param string $name
     * @param GenreKey|null $genreKey
     */
    public function __construct(string $name, GenreKey $genreKey = null)
    {
        $this->type = $name;
        $this->genreKey = $genreKey;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return GenreKey|null
     */
    public function getGenreKey()
    {
        return $this->genreKey;
    }

    /**
     * @param GenreKey $genreKey
     */
    public function setGenreKey(GenreKey $genreKey)
    {
        $this->genreKey = $genreKey;
    }
}
