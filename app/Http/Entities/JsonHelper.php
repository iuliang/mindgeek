<?php
declare(strict_types = 1);

namespace App\Http\Entities;

class JsonHelper
{
    /**
     * @param mixed $mixed
     * @return mixed
     */
    public static function utf8ize($mixed)
    {
        if (\is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = self::utf8ize($value);
            }
        } else if (\is_string($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }

    /**
     * @param $json
     * @param bool $asArray
     * @return mixed|null
     */
    public static function decode(string $json)
    {
        if ($json === null || $json === '') {
            return null;
        }

        return json_decode((string)$json, true);
    }
}
