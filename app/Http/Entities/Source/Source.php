<?php
declare(strict_types = 1);

namespace App\Http\Entities\Source;

class Source
{
    /** @var int */
    private $id;
    /** @var string */
    private $sourceURL;
    /** @var bool */
    private $processed;

    /**
     * Source constructor.
     * @param int $id
     * @param string $sourceURL
     * @param bool $processed
     */
    public function __construct(int $id, string $sourceURL, bool $processed)
    {
        $this->id = $id;
        $this->sourceURL = $sourceURL;
        $this->processed = $processed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSourceURL(): string
    {
        return $this->sourceURL;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->processed;
    }
}
