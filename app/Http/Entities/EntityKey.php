<?php
declare(strict_types = 1);

namespace App\Http\Entities;

class EntityKey
{
    /** @var string */
    private $keyType;
    /** @var mixed */
    private $keyId;

    /**
     * EntityKey constructor.
     * @param string $keyType
     * @param mixed $keyId
     */
    public function __construct(string $keyType, $keyId)
    {
        $this->keyType = $keyType;
        $this->keyId = $keyId;
    }

    /**
     * @return string
     */
    public function getKeyType(): string
    {
        return $this->keyType;
    }

    /**
     * @return mixed
     */
    public function getKeyId()
    {
        return $this->keyId;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->keyType . $this->keyId;
    }
}
