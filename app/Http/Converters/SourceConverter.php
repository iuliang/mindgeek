<?php
declare(strict_types = 1);

namespace App\Http\Converters;

use App\Http\Entities\Source\Source;
use Illuminate\Support\Collection;

class SourceConverter
{
    /**
     * @param $source
     * @return Source
     */
    public function createSource($source): Source
    {
        return new Source((int)$source->id, (string)$source->source_url, (bool)$source->processed === (bool)1);
    }

    public function convertSourceListToResult(Collection $sourceList)
    {
        $results = Collection::make();
        foreach ($sourceList as $source) {
            $results->push($this->createSource($source));
        }
        return $results;
    }
}
