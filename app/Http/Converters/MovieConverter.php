<?php
declare(strict_types = 1);

namespace App\Http\Converters;

use App\Http\Entities\Movies\CardImage;
use App\Http\Entities\Movies\Cast;
use App\Http\Entities\Movies\Director;
use App\Http\Entities\Movies\Gen;
use App\Http\Entities\Movies\KeyArtImage;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use App\Http\Entities\Movies\MovieInformation;
use App\Http\Entities\Movies\Video;
use DateTime;
use Illuminate\Support\Collection;

class MovieConverter
{
    public function convertDecodedToMovie(array $sourceArray): Movie
    {
        $body = $sourceArray['body'];

        $cardImages = Collection::make();
        foreach ($sourceArray['cardImages'] as $cardImage) {
            $cardImages->push(new CardImage($cardImage['url'], $cardImage['h'], $cardImage['w']));
        }

        $casts = Collection::make();
        foreach ($sourceArray['cast'] as $cast) {
            $casts->push(new Cast($cast['name']));
        }

        $cert = $sourceArray['cert'];
        $class = $sourceArray['class'];

        $directors = Collection::make();
        foreach ($sourceArray['directors'] as $director) {
            $directors->push(new Director($director['name']));
        }

        $duration = (int)$sourceArray['duration'];

        $genres = Collection::make();
        if (isset($sourceArray['genres'])) {
            foreach ($sourceArray['genres'] as $gen) {
                $genres->push(new Gen($gen));
            }
        }

        $headline = $sourceArray['headline'];
        $id = $sourceArray['id'];

        $keyArtImages = Collection::make();
        foreach ($sourceArray['keyArtImages'] as $keyArtImage) {
            $keyArtImages->push(new KeyArtImage($keyArtImage['url'], $keyArtImage['h'], $keyArtImage['w']));
        }

        $lastUpdated = DateTime::createFromFormat('Y-m-d', $sourceArray['lastUpdated']);
        $quote = $sourceArray['quote'] ?? '';

        if (isset($sourceArray['rating'])) {
            $rating = (int)$sourceArray['rating'];
        } else {
            $rating = 0;
        }

        $reviewAuthor = $sourceArray['reviewAuthor'] ?? '';
        $skyGoId = $sourceArray['skyGoId'] ?? '';
        $skyGoUrl = $sourceArray['skyGoUrl'] ?? '';
        $sum = $sourceArray['sum'];
        $synopsis = $sourceArray['synopsis'];
        $url = $sourceArray['url'];

        $videos = Collection::make();
        if (isset($sourceArray['videos'])) {
            foreach ($sourceArray['videos'] as $video) {
                $videos->push(new Video($video['title'], $video['url'], $video['type']));
            }
        }

        if (isset($sourceArray['viewingWindow'])) {
            $viewingWindow = http_build_query($sourceArray['viewingWindow'], '', ', ');
        } else {
            $viewingWindow = '';
        }
        $year = (int)$sourceArray['year'];

        return new Movie(
            $body,
            $cardImages,
            $casts,
            $cert,
            $class,
            $directors,
            $duration,
            $genres,
            $headline,
            $id,
            $keyArtImages,
            $lastUpdated,
            $quote,
            $rating,
            $reviewAuthor,
            $skyGoId,
            $skyGoUrl,
            $sum,
            $synopsis,
            $url,
            $videos,
            $viewingWindow,
            $year
        );
    }

    public function convertMovieRawToMovie(MovieInformation $movieInformation): Movie
    {
        $movieData = $movieInformation->getMovieData()->first();

        $body = $movieData->body;
        $cardImages = $movieInformation->getCardImages();
        $cast = $movieInformation->getMovieCast();
        $cert = $movieData->cert;
        $class = $movieData->class;
        $directors = $movieInformation->getMovieDirector();
        $duration = $movieData->duration;
        $genres = $movieInformation->getMovieGenre();
        $headline = $movieData->headline;
        $externalId = $movieData->external_id;
        $keyArtImages = $movieInformation->getKeyArtImages();
        $lastUpdated = DateTime::createFromFormat('Y-m-d H:i:s', $movieData->last_updated);
        $quote = $movieData->quote;
        $rating = $movieData->rating;
        $reviewAuthor = $movieData->review_author;
        $skyGoId = $movieData->sky_go_id;
        $skyGoUrl = $movieData->sky_go_url;
        $sum = $movieData->sum;
        $synopsis = $movieData->synopsys;
        $url = $movieData->url;
        $videos = $movieInformation->getVideos();
        $viewingWindow = $movieData->viewing_window;
        $year = $movieData->year;
        $movieKey = new MovieKey((int)$movieData->id);

        return new Movie(
            $body,
            $cardImages,
            $cast,
            $cert,
            $class,
            $directors,
            $duration,
            $genres,
            $headline,
            $externalId,
            $keyArtImages,
            $lastUpdated,
            $quote,
            $rating,
            $reviewAuthor,
            $skyGoId,
            $skyGoUrl,
            $sum,
            $synopsis,
            $url,
            $videos,
            $viewingWindow,
            $year,
            $movieKey
        );
    }
}
