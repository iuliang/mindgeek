<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\DirectorKey;
use Illuminate\Support\Collection;

interface IDirectorRepository
{
    /**
     * @return Collection
     */
    public function getDirectorsList(): Collection;

    /**
     * @param DirectorKey $directorKey
     * @return Collection
     */
    public function getMovieListForDirector(DirectorKey $directorKey): Collection;
}
