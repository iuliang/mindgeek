<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\CastKey;
use App\Http\Models\CastingDAO;
use Illuminate\Support\Collection;

class CastingRepository implements ICastingRepository
{
    /** @var CastingDAO */
    private $castingDAO;

    /**
     * SourceRepository constructor.
     * @param CastingDAO $castingDAO
     */
    public function __construct(CastingDAO $castingDAO)
    {
        $this->castingDAO = $castingDAO;
    }

    /**
     * @return Collection
     */
    public function getCastingList(): Collection
    {
        return $this->castingDAO->getCastingList();
    }

    /**
     * @param CastKey $castKey
     * @return Collection
     */
    public function getMovieListForCasting(CastKey $castKey): Collection
    {
        return $this->castingDAO->getMovieListForCasting($castKey);
    }
}
