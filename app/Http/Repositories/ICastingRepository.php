<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\CastKey;
use Illuminate\Support\Collection;

interface ICastingRepository
{
    /**
     * @return Collection
     */
    public function getCastingList(): Collection;

    /**
     * @param CastKey $castKey
     * @return Collection
     */
    public function getMovieListForCasting(CastKey $castKey): Collection;
}
