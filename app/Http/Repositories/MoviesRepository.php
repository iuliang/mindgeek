<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use App\Http\Entities\Movies\MovieInformation;
use App\Http\Models\CastingDAO;
use App\Http\Models\DirectorDAO;
use App\Http\Models\GenresDAO;
use App\Http\Models\MediaDAO;
use App\Http\Models\MoviesDAO;
use Illuminate\Support\Collection;

class MoviesRepository implements IMoviesRepository
{
    /** @var MoviesDAO */
    private $moviesDAO;
    /** @var CastingDAO */
    private $castingDAO;
    /** @var DirectorDAO */
    private $directorDAO;
    /** @var GenresDAO */
    private $genresDAO;
    /** @var MediaDAO */
    private $mediaDAO;

    /**
     * SourceRepository constructor.
     * @param MoviesDAO $moviesDAO
     * @param CastingDAO $castingDAO
     * @param DirectorDAO $directorDAO
     * @param GenresDAO $genresDAO
     * @param MediaDAO $mediaDAO
     */
    public function __construct(
        MoviesDAO $moviesDAO,
        CastingDAO $castingDAO,
        DirectorDAO $directorDAO,
        GenresDAO $genresDAO,
        MediaDAO $mediaDAO
    ) {
        $this->moviesDAO = $moviesDAO;
        $this->castingDAO = $castingDAO;
        $this->directorDAO = $directorDAO;
        $this->genresDAO = $genresDAO;
        $this->mediaDAO = $mediaDAO;
    }

    /**
     * @return Collection
     */
    public function getMoviesList(): Collection
    {
        return $this->moviesDAO->getMoviesList();
    }

    /**
     * @param MovieKey $movieKey
     * @return MovieInformation
     */
    public function getMovieInformation(MovieKey $movieKey): MovieInformation
    {
        $rawMovieData = $this->moviesDAO->getMovie($movieKey);
        $movieCast = $this->castingDAO->getCastingListForMovie($movieKey);
        $movieDirector = $this->directorDAO->getDirectorListForMovie($movieKey);
        $movieGenre = $this->genresDAO->getGenreListForMovie($movieKey);
        $cardImages = $this->mediaDAO->getCardImagesForMovie($movieKey);
        $keyArtImages = $this->mediaDAO->getKeyArtImagesForMovie($movieKey);
        $videos = $this->mediaDAO->getVideosForMovie($movieKey);

        return new MovieInformation(
            $rawMovieData,
            $movieCast,
            $movieDirector,
            $movieGenre,
            $cardImages,
            $keyArtImages,
            $videos
        );
    }

    /**
     * @param Movie $movie
     */
    public function saveMovie(Movie $movie)
    {
        $this->moviesDAO->saveMovie($movie);
        $this->castingDAO->saveCasting($movie);
        $this->directorDAO->saveDirector($movie);
        $this->genresDAO->saveGenres($movie);
    }

    /**
     * @param Movie $movie
     * @return bool
     */
    public function movieExists(Movie $movie): bool
    {
        return $this->moviesDAO->movieExists($movie);
    }
}
