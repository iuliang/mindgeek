<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\GenreKey;
use Illuminate\Support\Collection;

interface IGenreRepository
{
    /**
     * @return Collection
     */
    public function getGenresList(): Collection;
    /**
     * @param GenreKey $genreKey
     * @return Collection
     */
    public function getMovieListForGenre(GenreKey $genreKey): Collection;
}
