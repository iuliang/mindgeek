<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\CastingListKey;
use App\Http\Entities\Movies\Keys\CastKey;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CachedCastingRepository implements ICastingRepository
{
    const CACHE_VALABILITY = 2;

    /** @var CastingRepository */
    private $castingRepository;

    /**
     * SourceRepository constructor.
     * @param CastingRepository $castingRepository
     */
    public function __construct(CastingRepository $castingRepository)
    {
        $this->castingRepository = $castingRepository;
    }

    /**
     * @return Collection
     */
    public function getCastingList(): Collection
    {
        $castingListKey = new CastingListKey();
        $castingList = Cache::get((string)$castingListKey);
        if ($castingList === null) {
            $castingList = $this->castingRepository->getCastingList();
            Cache::put((string)$castingListKey, $castingList, self::CACHE_VALABILITY);
        }

        return $castingList;
    }

    /**
     * @param CastKey $castKey
     * @return Collection
     */
    public function getMovieListForCasting(CastKey $castKey): Collection
    {
        $movies = Cache::get((string)$castKey);
        if ($movies === null) {
            $movies = $this->castingRepository->getMovieListForCasting($castKey);
            Cache::put((string)$castKey, $movies, self::CACHE_VALABILITY);
        }

        return $movies;
    }
}
