<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Movie;
use App\Http\Models\MediaDAO;

class MediaRepository
{
    /** @var MediaDAO */
    private $mediaDAO;

    /**
     * SourceRepository constructor.
     * @param MediaDAO $mediaDAO
     */
    public function __construct(
        MediaDAO $mediaDAO
    ) {
        $this->mediaDAO = $mediaDAO;
    }

    /**
     * @param Movie $movie
     */
    public function saveCardImages(Movie $movie)
    {
        $this->mediaDAO->saveCardImages($movie);
    }

    /**
     * @param Movie $movie
     */
    public function saveKeyArtImages(Movie $movie)
    {
        $this->mediaDAO->saveKeyArtImages($movie);
    }

    /**
     * @param Movie $movie
     */
    public function saveVideos(Movie $movie)
    {
        $this->mediaDAO->saveVideos($movie);
    }
}
