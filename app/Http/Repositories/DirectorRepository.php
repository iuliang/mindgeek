<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\DirectorKey;
use App\Http\Models\DirectorDAO;
use Illuminate\Support\Collection;

class DirectorRepository implements IDirectorRepository
{
    /** @var DirectorDAO */
    private $directorDAO;

    /**
     * SourceRepository constructor.
     * @param DirectorDAO $directorDAO
     */
    public function __construct(DirectorDAO $directorDAO)
    {
        $this->directorDAO = $directorDAO;
    }

    /**
     * @return Collection
     */
    public function getDirectorsList(): Collection
    {
        return $this->directorDAO->getDirectorsList();
    }

    /**
     * @param DirectorKey $directorKey
     * @return Collection
     */
    public function getMovieListForDirector(DirectorKey $directorKey): Collection
    {
        return $this->directorDAO->getMovieListForDirector($directorKey);
    }
}
