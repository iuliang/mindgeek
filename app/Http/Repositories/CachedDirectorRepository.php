<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\DirectoriesListKey;
use App\Http\Entities\Movies\Keys\DirectorKey;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CachedDirectorRepository implements IDirectorRepository
{
    const CACHE_VALABILITY = 2;

    /** @var DirectorRepository */
    private $directorRepository;

    /**
     * SourceRepository constructor.
     * @param DirectorRepository $directorRepository
     */
    public function __construct(DirectorRepository $directorRepository)
    {
        $this->directorRepository = $directorRepository;
    }

    /**
     * @return Collection
     */
    public function getDirectorsList(): Collection
    {
        $directoriesListKey = new DirectoriesListKey();
        $directorsList = Cache::get((string)$directoriesListKey);
        if ($directorsList === null) {
            $directorsList = $this->directorRepository->getDirectorsList();
            Cache::put((string)$directoriesListKey, $directorsList, self::CACHE_VALABILITY);
        }

        return $directorsList;
    }

    /**
     * @param DirectorKey $directorKey
     * @return Collection
     */
    public function getMovieListForDirector(DirectorKey $directorKey): Collection
    {
        $movies = Cache::get((string)$directorKey);
        if ($movies === null) {
            $movies = $this->directorRepository->getMovieListForDirector($directorKey);
            Cache::put((string)$directorKey, $movies, self::CACHE_VALABILITY);
        }

        return $movies;
    }
}
