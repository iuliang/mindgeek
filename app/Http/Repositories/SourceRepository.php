<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Source\Source;
use App\Http\Models\SourceDAO;
use Illuminate\Support\Collection;

class SourceRepository
{
    /** @var SourceDAO */
    private $sourceDAO;

    /**
     * SourceRepository constructor.
     * @param SourceDAO $sourceDAO
     */
    public function __construct(SourceDAO $sourceDAO)
    {
        $this->sourceDAO = $sourceDAO;
    }

    /**
     * @return Collection
     */
    public function getSources(): Collection
    {
        return $this->sourceDAO->getSources();
    }

    /**
     * @param int $sourceId
     * @return Collection
     */
    public function getSource(int $sourceId): Collection
    {
        return $this->sourceDAO->getSource($sourceId);
    }

    /**
     * @param Source $source
     */
    public function updateSourceStatus(Source $source) {
        $this->sourceDAO->updateSourceStatus($source);
    }
}
