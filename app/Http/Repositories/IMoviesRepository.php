<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\CastKey;
use App\Http\Entities\Movies\Keys\DirectorKey;
use App\Http\Entities\Movies\Keys\GenreKey;
use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Movie;
use App\Http\Entities\Movies\MovieInformation;
use Illuminate\Support\Collection;

interface IMoviesRepository
{
    /**
     * @return Collection
     */
    public function getMoviesList(): Collection;

    /**
     * @param MovieKey $movieKey
     * @return MovieInformation
     */
    public function getMovieInformation(MovieKey $movieKey): MovieInformation;
}
