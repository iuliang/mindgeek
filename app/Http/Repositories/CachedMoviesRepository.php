<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Entities\Movies\Keys\MoviesListKey;
use App\Http\Entities\Movies\Movie;
use App\Http\Entities\Movies\MovieInformation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CachedMoviesRepository implements IMoviesRepository
{
    const CACHE_VALABILITY = 2;

    /** @var MoviesRepository */
    private $movieRepository;

    /**
     * SourceRepository constructor.
     * @param MoviesRepository $moviesRepository
     */
    public function __construct(
        MoviesRepository $moviesRepository
    ) {
        $this->movieRepository = $moviesRepository;
    }

    /**
     * @param Movie $movie
     */
    public function saveMovie(Movie $movie)
    {
        $this->movieRepository->saveMovie($movie);
    }

    /**
     * @return Collection
     */
    public function getMoviesList(): Collection
    {
        $moviesListKey = new MoviesListKey();
        $moviesList = Cache::get((string)$moviesListKey);
        if ($moviesList === null) {
            $moviesList = $this->movieRepository->getMoviesList();
            Cache::put((string)$moviesListKey, $moviesList, self::CACHE_VALABILITY);
        }

        return $moviesList;
    }

    /**
     * @param MovieKey $movieKey
     * @return MovieInformation
     */
    public function getMovieInformation(MovieKey $movieKey): MovieInformation
    {
        $movieInformation = Cache::get((string)$movieKey);
        if ($movieInformation === null) {
            $movieInformation = $this->movieRepository->getMovieInformation($movieKey);
            Cache::put((string)$movieKey, $movieInformation, self::CACHE_VALABILITY);
        }

        return $movieInformation;
    }
}
