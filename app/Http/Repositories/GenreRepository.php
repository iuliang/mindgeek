<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\GenreKey;
use App\Http\Models\GenresDAO;
use Illuminate\Support\Collection;

class GenreRepository implements IGenreRepository
{
    /** @var GenresDAO */
    private $genresDAO;

    /**
     * SourceRepository constructor.
     * @param GenresDAO $genresDAO
     */
    public function __construct(GenresDAO $genresDAO)
    {
        $this->genresDAO = $genresDAO;
    }

    /**
     * @return Collection
     */
    public function getGenresList(): Collection
    {
        return $this->genresDAO->getGenresList();
    }

    /**
     * @param GenreKey $genreKey
     * @return Collection
     */
    public function getMovieListForGenre(GenreKey $genreKey): Collection
    {
        return $this->genresDAO->getMovieListForGenre($genreKey);
    }
}
