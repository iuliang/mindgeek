<?php
declare(strict_types = 1);

namespace App\Http\Repositories;

use App\Http\Entities\Movies\Keys\GenreKey;
use App\Http\Entities\Movies\Keys\GenresListKey;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CachedGenreRepository implements IGenreRepository
{
    const CACHE_VALABILITY = 2;

    /** @var GenreRepository */
    private $genreRepository;

    /**
     * SourceRepository constructor.
     * @param GenreRepository $genreRepository
     */
    public function __construct(GenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * @return Collection
     */
    public function getGenresList(): Collection
    {
        $genresListKey = new GenresListKey();
        $genresList = Cache::get((string)$genresListKey);
        if ($genresList === null) {
            $genresList = $this->genreRepository->getGenresList();
            Cache::put((string)$genresListKey, $genresList, self::CACHE_VALABILITY);
        }

        return $genresList;
    }

    /**
     * @param GenreKey $genreKey
     * @return Collection
     */
    public function getMovieListForGenre(GenreKey $genreKey): Collection
    {
        $movies = Cache::get((string)$genreKey);
        if ($movies === null) {
            $movies = $this->genreRepository->getMovieListForGenre($genreKey);
            Cache::put((string)$genreKey, $movies, self::CACHE_VALABILITY);
        }

        return $movies;
    }
}
