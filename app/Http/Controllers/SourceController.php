<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Converters\SourceConverter;
use App\Http\Repositories\MediaRepository;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Converters\MovieConverter;
use App\Http\Entities\JsonHelper;
use App\Http\Repositories\MoviesRepository;
use App\Http\Repositories\SourceRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Http\Redirector;

class SourceController extends BaseController
{
    /** @var SourceRepository */
    private $sourceRepository;
    /** @var MoviesRepository */
    private $moviesRepository;
    /** @var SourceConverter */
    private $sourceConverter;
    /** @var MovieConverter */
    private $movieConverter;
    /** @var MediaRepository */
    private $mediaRepository;

    /**
     * SourceController constructor.
     * @param SourceRepository $sourceRepository
     * @param MoviesRepository $moviesRepository
     * @param SourceConverter $sourceConverter
     * @param MovieConverter $movieConverter
     * @param MediaRepository $mediaRepository
     */
    public function __construct(
        SourceRepository $sourceRepository,
        MoviesRepository $moviesRepository,
        SourceConverter $sourceConverter,
        MovieConverter $movieConverter,
        MediaRepository $mediaRepository
    ) {
        $this->sourceRepository = $sourceRepository;
        $this->moviesRepository = $moviesRepository;
        $this->sourceConverter = $sourceConverter;
        $this->movieConverter = $movieConverter;
        $this->mediaRepository = $mediaRepository;
    }

    public function index()
    {
        $sourcesRawList = $this->sourceRepository->getSources();
        $result = $this->sourceConverter->convertSourceListToResult($sourcesRawList);

        return view('source', ['sources' => $result]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function importStatic(Request $request) {
        set_time_limit(300);

        $sourceId = $request->query('id');
        if ($sourceId === null) {
            return redirect('sources');
        }

        $sourceFind = $this->sourceRepository->getSource((int)$sourceId);
        if ($sourceFind->first() !== null) {
            $source = $this->sourceConverter->createSource($sourceFind->first());
            if ($source->getSourceURL()) {
                $sourceJson = file_get_contents($source->getSourceURL());
                $sourceJsonSanitized = JsonHelper::utf8ize($sourceJson);
                $decodedJson = JsonHelper::decode($sourceJsonSanitized);

                foreach ($decodedJson as $decodedMovie) {
                    try{
                        $movie = $this->movieConverter->convertDecodedToMovie($decodedMovie);
                        $movieIsAlreadySaved = $this->moviesRepository->movieExists($movie);
                        if (!$movieIsAlreadySaved) {
                            $this->moviesRepository->saveMovie($movie);
                        }
                        $this->mediaRepository->saveCardImages($movie);
                        $this->mediaRepository->saveKeyArtImages($movie);
                        $this->mediaRepository->saveVideos($movie);
                    } catch (\Throwable $exception) {
                        Log::warning('Error on movie: ' . $decodedMovie['id']);
                    }
                }
                $this->sourceRepository->updateSourceStatus($source);
                return redirect('movies');
            }
        }

        return redirect('sources');
    }
}
