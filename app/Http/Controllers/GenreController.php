<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Entities\Movies\Keys\GenreKey;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Repositories\CachedGenreRepository;
use App\Http\Repositories\IGenreRepository;
use Illuminate\Http\Request;

class GenreController extends BaseController
{
    /** @var CachedGenreRepository */
    private $cachedGenreRepository;

    /**
     * SourceController constructor.
     * @param IGenreRepository $genreRepository
     */
    public function __construct(
        IGenreRepository $genreRepository
    ) {
        $this->cachedGenreRepository = $genreRepository;
    }

    public function index(Request $request)
    {
        $genreId = $request->query('id');
        if ($genreId === null) {
            $genreList = $this->cachedGenreRepository->getGenresList();
            return view('genre_list', ['genresList' => $genreList]);
        }

        $genreKey = new GenreKey((int)$genreId);
        $movieListForGenre = $this->cachedGenreRepository->getMovieListForGenre($genreKey);

        return view('genre_list_details', ['list' => $movieListForGenre]);
    }
}
