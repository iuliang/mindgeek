<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Entities\Movies\Keys\MovieKey;
use App\Http\Repositories\CachedMoviesRepository;
use App\Http\Repositories\IMoviesRepository;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Converters\MovieConverter;
use Illuminate\Http\Request;

class MovieController extends BaseController
{
    /** @var CachedMoviesRepository */
    private $cachedMoviesRepository;
    /** @var MovieConverter */
    private $movieConverter;

    /**
     * SourceController constructor.
     * @param IMoviesRepository $moviesRepository
     * @param MovieConverter $movieConverter
     */
    public function __construct(
        IMoviesRepository $moviesRepository,
        MovieConverter $movieConverter
    ) {
        $this->cachedMoviesRepository = $moviesRepository;
        $this->movieConverter = $movieConverter;
    }

    public function index(Request $request)
    {
        $message = null;
        $movieId = $request->query('id');
        if ($movieId !== null) {
            $movieKey = new MovieKey((int)$movieId);
            $movieInformation = $this->cachedMoviesRepository->getMovieInformation($movieKey);

            if ($movieInformation->getMovieData()->first() !== null) {
                $movie = $this->movieConverter->convertMovieRawToMovie($movieInformation);
                return view('movie', [
                    'movie' => $movie
                ]);
            }
            $message = 'Movie can not be found!';
        }

        $moviesList = $this->cachedMoviesRepository->getMoviesList();
        if ($moviesList->count() === 0) {
            return redirect('sources');
        }
        $data = ['moviesList' => $moviesList];
        if ($message !== null) {
            $data['message'] = $message;
        }

        return view('movies_list', $data);
    }
}
