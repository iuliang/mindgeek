<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Entities\Movies\Keys\DirectorKey;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Repositories\CachedDirectorRepository;
use App\Http\Repositories\IDirectorRepository;
use Illuminate\Http\Request;

class DirectorController extends BaseController
{
    /** @var CachedDirectorRepository */
    private $cachedDirectorRepository;

    /**
     * SourceController constructor.
     * @param IDirectorRepository $directorRepository
     */
    public function __construct(
        IDirectorRepository $directorRepository
    ) {
        $this->cachedDirectorRepository = $directorRepository;
    }

    public function index(Request $request)
    {
        $directorId = $request->query('id');
        if ($directorId === null) {
            $directorList = $this->cachedDirectorRepository->getDirectorsList();
            return view('directors_list', ['directorsList' => $directorList]);
        }

        $directorKey = new DirectorKey((int)$directorId);
        $movieListForDirector = $this->cachedDirectorRepository->getMovieListForDirector($directorKey);

        return view('directors_list_details', ['list' => $movieListForDirector]);
    }
}
