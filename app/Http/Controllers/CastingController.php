<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Entities\Movies\Keys\CastKey;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Repositories\CachedMoviesRepository;
use App\Http\Repositories\ICastingRepository;
use Illuminate\Http\Request;

class CastingController extends BaseController
{
    /** @var CachedMoviesRepository */
    private $cachedMoviesRepository;

    /**
     * SourceController constructor.
     * @param ICastingRepository $castingRepository
     */
    public function __construct(
        ICastingRepository $castingRepository
    ) {
        $this->cachedMoviesRepository = $castingRepository;
    }

    public function index(Request $request)
    {
        $castingId = $request->query('id');
        if ($castingId === null) {
            $castingList = $this->cachedMoviesRepository->getCastingList();
            return view('casting_list', ['castingList' => $castingList]);
        }

        $castKey = new CastKey((int)$castingId);
        $movieListForCasting = $this->cachedMoviesRepository->getMovieListForCasting($castKey);

        return view('casting_list_details', ['list' => $movieListForCasting]);
    }
}
