@extends('index')

@section('title')
    Movie
@endsection
@section('subject')
    Movie
@endsection
@section('breadcrumb', 'Movie');
@section('body_content')
    <div class="box">
        <div class="box-header">
            @if(!empty($message))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{  $message }}
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">


            <div class="col-md-7">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-header"><b>{{ $movie->getHeadline() }}</b></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-condensed">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Information:</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Headline</td>
                                <td>
                                    <p><b>{{ $movie->getHeadline() }}</b></p>
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Cast</td>
                                <td>
                                    @foreach ($movie->getCasts() as $cast)
                                        <p><a href="/casting?id={{ $cast->casting_id }}" >{{ $cast->name }}</a></p>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Cert</td>
                                <td>
                                    <p>{{ $movie->getCert() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Class</td>
                                <td>
                                    <p>{{ $movie->getClass() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Directors</td>
                                <td>
                                    @foreach ($movie->getDirectors() as $director)
                                        <p><a href="/directors?id={{ $director->director_id }}" >{{ $director->name }}</a></p>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Duration</td>
                                <td>
                                    <p>{{ $movie->getDuration() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Genres</td>
                                <td>
                                    @foreach ($movie->getGenres() as $genre)
                                        <p><a href="/genres?id={{ $genre->genre_id }}" >{{ $genre->type }}</a></p>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Last Updated</td>
                                <td>
                                    <p>{{ $movie->getLastUpdated()->format('d/m/Y') }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Quote</td>
                                <td>
                                    <p class="text-justify">{{ $movie->getQuote() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Rating</td>
                                <td>
                                    <p>{{ $movie->getRating() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td>Review Author</td>
                                <td>
                                    <p>{{ $movie->getReviewAuthor() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td>SkyGo Id</td>
                                <td>
                                    <p>{{ $movie->getSkyGoId() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>13.</td>
                                <td>SkyGo Url</td>
                                <td>
                                    <p>
                                        <a href="{{ $movie->getSkyGoUrl() }}" >SkyGo Url</a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>14.</td>
                                <td>Sum</td>
                                <td>
                                    <p>{{ $movie->getSum() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>15.</td>
                                <td>Synopsis</td>
                                <td>
                                    <p class="text-justify">{{ $movie->getSynopsis() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td>Url</td>
                                <td>
                                    <p>
                                        <a href="{{ $movie->getUrl() }}" ><b>{{ $movie->getHeadline() }}</b> more info...</a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td>Viewing Window</td>
                                <td>
                                    <p>{{ $movie->getViewingWindow() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td>Year</td>
                                <td>
                                    <p>{{ $movie->getYear() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td>Id</td>
                                <td>
                                    <p>{{ $movie->getExternalId() }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td>Body</td>
                                <td>
                                    <p class="text-justify">{{ $movie->getBody() }}</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            @if ($movie->getVideos()->first() !== null)
                <div class="col-md-3">
                    <video width="320" height="240" controls="">
                        <source src="{{ $movie->getVideos()->first()->getUrl() }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <p>This is a trailer</p>
                </div>
            @endif
            @if ($movie->getCardImages()->first() !== null)
                <div class="col-md-3 image">
                    <img width="320" height="240" src="{{ $movie->getCardImages()->first()->getUrl() }}" />
                    <p>This is a card image</p>
                </div>
            @endif
            @if ($movie->getKeyArtImages()->first() !== null)
                <div class="col-md-3 image">
                    <img width="320" height="240" src="{{ $movie->getKeyArtImages()->first()->getUrl() }}" />
                    <p>This is a key art image</p>
                </div>
            @endif



        </div>
        <!-- /.box-body -->
    </div>
@endsection