@extends('index')

@section('title', 'Movies')
@section('subject', 'Movies')
@section('breadcrumb', 'Movies');
@section('body_content')
    <div class="box">
        <div class="box-header">
            {{--<h3 class="box-title">Data table</h3>--}}
            @if(!empty($message))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{  $message }}
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Movie Title</th>
                    <th>Movie Rating</th>
                    <th>Movie Year</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($moviesList as $movie)
                    <tr>
                        <td>
                            <a href="/movies?id={{ $movie->id }}" >{{ $movie->headline }}</a>
                        </td>
                        <td>
                            {{ $movie->rating }}
                        </td>
                        <td>
                            {{ $movie->year }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection