@extends('index')

@section('title')
    Genres
@endsection
@section('subject')
    Genres
@endsection
@section('breadcrumb', 'Genres');
@section('body_content')
    <div class="box">
        <div class="box-header">
            {{--<h3 class="box-title">Data table</h3>--}}
            @if(!empty($message))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{  $message }}
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Genre ID</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($genresList as $gen)
                    <tr>
                        <td>
                            <a href="/genres?id={{ $gen->id }}" >{{ $gen->id }}</a>
                        </td>
                        <td>
                            {{ $gen->type }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection