@extends('index')

@section('title', 'Sources')
@section('subject', 'Sources')
@section('body_content')
    <div class="box">
        <div class="box-header">
            @if(!empty($message))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{  $message }}
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Import Source</th>
                    <th>Source URL</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($sources as $source)
                    <tr>
                        <td>
                            @if ($source->isProcessed() === false)
                                <a class="btn btn-app" href="/sources/import?id={{ $source->getId() }}">
                                    <i class="fa fa-play"></i> Import
                                </a>
                            @else
                                <p></p>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="callout callout-info">
                                <h4>{{ $source->getSourceURL() }}</h4>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection