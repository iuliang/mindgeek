@extends('index')

@section('title')
    Directors
@endsection
@section('subject')
    Directors
@endsection
@section('breadcrumb', 'Directors');
@section('body_content')
    <div class="box">
        <div class="box-header">
            {{--<h3 class="box-title">Data table</h3>--}}
            @if(!empty($message))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    {{  $message }}
                </div>
            @endif
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Director ID</th>
                    <th>Director Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($directorsList as $director)
                    <tr>
                        <td>
                            <a href="/directors?id={{ $director->id }}" >{{ $director->name }}</a>
                        </td>
                        <td>
                            {{ $director->name }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection